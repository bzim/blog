#!/usr/bin/env sh

set -e

if [ -f cobalt ]
then
    exit 0
fi

curl -LO https://github.com/cobalt-org/cobalt.rs/releases/download/v0.18.3/cobalt-v0.18.3-x86_64-unknown-linux-gnu.tar.gz

tar -xf cobalt-v0.18.3-x86_64-unknown-linux-gnu.tar.gz
